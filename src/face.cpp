#include "interface.h"
#include "aip-cpp-sdk/face.h" //只能放在cpp文件中包含，否则会有重复定义的错误

using namespace cv;
using namespace std;

Face::Face(string& model)
{
    classifier.load(model);
}

void Face::detect(Mat image)
{
    Rect face(0,0,0,0);
    cvtColor(image, image, CV_BGR2GRAY);
    equalizeHist(image, image);
    vector<Rect> faces;
    classifier.detectMultiScale(image, faces, 1.1, 3, 0, Size(50, 50));
    if (faces.size())
    {
        face = faces[0];
    }
    emit detected(face);
}

void Face::recognize(Mat face)
{
    Employee e;
    aip::Face client("18003626",
                     "kIGVsMnZEKqCfLfnl9op20cq",
                     "udOcOUcZ1sRVG40sHTm5x4FzjiUxmpUh");
    vector<unsigned char> buf;
    imencode(".jpg", face, buf);
    string faceimg = aip::base64_encode((char*)buf.data(), buf.size());
    Json::Value json = client.search(faceimg, "BASE64", "group1", aip::null);
    if (json["error_code"].asInt())
    {
        cout << json["error_msg"] << endl;
        return;
    }
    e.id = json["result"]["user_list"][0]["user_id"].asString();
    e.depart_id = json["result"]["user_list"][0]["group_id"].asString();
    e.info = json["result"]["user_list"][0]["user_info"].asString();
    e.score = json["result"]["user_list"][0]["score"].asDouble();
    if (e.score < 90.0)
    {
        cout << e.score << endl;
        return;
    }
    Json::StyledWriter writer;
    cout << writer.write(json) << endl;
    emit recognized(e);
}
