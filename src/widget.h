#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QElapsedTimer>
#include <QThread>
#include <opencv2/opencv.hpp>
#include <interface.h>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void update();
    void updateFace(cv::Rect);
    void updateInfo(Employee);

signals:
    void doDetect(cv::Mat);
    void doRecognize(cv::Mat);

private:
    Ui::Widget *ui;
    cv::VideoCapture cam;
    QTimer* timer;
    Face* face;
    long frame;
    cv::Rect range;
    int hit;
    Database* db;
    QElapsedTimer t;
    QThread facethread;
};

#endif // WIDGET_H
