# 模块划分

1. 视频采集 -- V4L，OpenCV
2. 人脸检测 -- 离线检测，识别照片中的哪一部分是人脸
3. 人脸识别 -- 在线识别，对比采集的照片中和事先录入的照片是否是同一个人
4. 入库 -- 记录到考勤机本地的数据库中
5. 信息显示 -- 直接显示到屏幕上

硬件：摄像头（分辨率），ARM Cortex A系列开发板，本地存储（Flash容量），TFT液晶屏幕（分辨率，大小），内存容量，网络连接方式（可选，有线/无线，带宽）

raspberry pi 树莓派

软件：Linux系统（Raspbian/debian10），OpenCV（3.2版本），云服务

模块：

1. 考勤机图形界面：GUI+视频采集+信息显示
2. 人脸识别：人脸检测+百度API调用
3. 数据库客户端：获取系统时间+建库+入库
4. 数据库服务器
5. 考勤员客户端

## 用户故事

### 打卡

```mermaid
  sequenceDiagram
    图形界面->>人脸识别: 每200ms进行一次人脸检测
    图形界面->>人脸识别: 连续5次检测到人脸，进行人脸识别
    人脸识别->>数据库: 将打卡人工号和打卡时间记录到数据库
    人脸识别->>图形界面: 将打卡信息发送到图形界面显示
    图形界面->>数据库: 根据工号查询员工姓名
```

图形界面：

1. 考勤机启动后，显示用户界面，公司的Logo，摄像头采集的现场视频。（8）
2. 每200毫秒调用一次人脸识别模块提供的函数，检测捕获的图片中是否存在人脸。（1）
3. 如果连续5次都检测到人脸，我们认为是一个人的人脸，调用人脸识别模块提供的函数，识别最后一次检测到的人脸，并提供回调函数。（2）
4. 在回调函数中，人脸识别成功获取到工号（1）
5. 在回调函数中，获取系统当前时间，作为打卡时间（1）
6. 在回调函数中，调用数据库模块提供的接口，将工号和打卡时间记录到数据库（1）
7. 在回调函数中，调用数据库模块提供的接口，获取用户姓名（1）
8. 在回调函数中，将工号，姓名，打卡时间显示到界面上，持续至少1秒钟。（8）

人脸识别：

1. 提供人脸检测函数供图形界面模块调用（8）
2. 提供人脸识别函数，创建线程，发送人脸图片到百度云，等待百度云返回结果。（8）
3. 百度云返回结果后，解析json结果，回调图形界面注册的处理函数。（8）
4. 申请百度云账号，创建人脸识别应用和人脸库。（8）

数据库：

1. 启动后打开数据库，如果数据库不存在则重新创建。（8）
2. 提供打卡记录函数，将工号和打卡时间记录到数据库。（4）
3. 提供用户名查询函数，根据工号找到姓名。（8）

总工时：23+24+20 = 67/8 = 9人天/0.8 = 12

### 支持多考勤机

![image-20200410163137567](function%20design.assets/image-20200410163137567.png)

数据库客户端：

1. 考勤机启动后，连接到数据库服务器进行认证，密码保存到配置文件中。（8）
2. 将考勤服务器的时间同步到考勤机。（8）
3. 认证失败后，在界面上显示认证失败，暂停使用。（4）
4. 提供打卡记录函数，将工号和打卡时间发送给数据库服务器。（8）
5. 提供用户名查询函数，根据工号找到姓名。（8）

考勤服务器：

1. 创建TCP并发服务器，端口号8888。（8）
2. 处理考勤机的认证消息。（8）
3. 处理考勤机的打卡消息。（8）
4. 处理考勤机的查询消息（通过工号查询用户信息）（8）。

### 新员工注册

考勤员客户端：

1. 输入密码，登录到考勤服务器。（8）
2. 创建界面，登记员工信息（照片，姓名，工号，电话，性别，入职时间，职位，部门，邮箱地址）。（16）
3. 点击确认，将员工信息添加到数据库。（8）
4. 如果注册成功，显示成功对话框。（4）
5. 如果注册失败，显示失败原因对话框。（已注册/必选信息没有填写）（4）
6. 将人脸添加到人脸库。（8）

考勤服务器：

1. 处理考勤员客户端的认证消息。（4）
2. 处理员工注册消息。（8）

### 离职员工注销

考勤员客户端：

1. 输入密码，登录到考勤服务器。（8）
2. 创建界面，通过工号查询员工信息。（16）
3. 点击删除按钮，弹出确认对话框，再次点击确认，将员工信息删除。（8）
4. 如果删除成功，显示成功对话框。（4）
5. 如果查询失败，显示失败原因对话框。（已注销）（4）
6. 将人脸从人脸库删除。（8）

考勤服务器：

1. 处理考勤员客户端的删除消息。（4）



## 什么是OpenCV

OpenCV Intel公司发布的一款计算机视觉库，用于图像分析。

## 用OpenCV做什么

调用摄像头驱动采集图像，对图像进行预处理，用于人脸检测的物体检测算法（基于决策树的机器学习算法）

## 安装Raspbian

1. （可选）修改软件源 https://mirrors.tuna.tsinghua.edu.cn/help/debian/
2. 安装vim

```bash
sudo apt install vim
```

3. 安装opencv开发软件包

```bash
sudo apt update
sudo apt install libopencv-dev
```

## 打开SSH服务

```bash
sudo raspi-config
```



## OpenCV读取摄像头

```c++
#include <opencv2/opencv.hpp>

using namespace cv;

int main()
{
    VideoCapture cam(0); //创建摄像头对象
    namedWindow("Camera"); //创建名字为Camera的窗口
    
    Mat image;
    
    while(1)
    {
        cam >> image; //从摄像头读取一帧图像
    	imshow("Camera", image); //在Camera窗口显示摄像头捕获到的图像
    	if (waitKey(10) != 255) //延时10毫秒，判断是否有按键按下，如果按下结束程序
        {
            break;
        }
    }
}
```

编译命令：

```bash
g++ cam.cpp -lopencv_core -lopencv_videoio -lopencv_highgui 
```



## OpenCV人脸检测

```C++
#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;

int main()
{
	VideoCapture cam(0);
    //创建级联分类器，并加载人脸检测模型文件
	CascadeClassifier classifier("/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml");

	namedWindow("Camera");

	Mat image;
	Mat gray; //保存转换后的灰度图像

	while(1)
	{
		cam >> image;
		cvtColor(image, gray, COLOR_BGR2GRAY);  //将3通道的彩色图像转换为单通道灰度图像
		equalizeHist(gray, gray);  //对图像进行均衡化处理，提高图像对比度，优化分类器的效果
		vector<Rect> faces;  //定义数组，保存检测到的人脸区域
		classifier.detectMultiScale(gray, faces); //检测人脸
		if (faces.size())  //是否找到人脸
		{
			rectangle(image, faces[0], CV_RGB(255, 0, 0)); //将找到的人脸区域用矩形框标记出来
		}
		imshow("Camera", image);
		if (waitKey(40) != 255)
		{
			break;
		}
	}
}

```

编译命令：

```bash
g++ cam.cpp -lopencv_core -lopencv_videoio -lopencv_highgui -lopencv_imgproc -lopencv_objdetect
```

## REST API

curl是一个命令行下的HTTP客户端，可以通过命令的方式调用REST API。

```bash
curl -X GET http://coronavirus-tracker-api.herokuapp.com/v2/locations/225
```



通过C代码调用REST API

1. 安装libcurl函数库

```bash
sudo apt install libcurl4-gnutls-dev
```

2. 开发HTTP客户端程序

```c++
#include <curl/curl.h> //包含libcurl库的头文件

int main()
{
	CURL* client = curl_easy_init();  //初始化libcurl库，创建客户端句柄，类似于fopen

	curl_easy_setopt(client, CURLOPT_URL, "http://coronavirus-tracker-api.herokuapp.com/v2/locations/225");  //设置要访问的REST API地址
	curl_easy_perform(client); //默认执行GET方法，并将服务器返回的数据打印到标准输出

	curl_easy_cleanup(client);  //释放客户端句柄，类似于fclose
}

```

3. libcurl的API文档：https://curl.haxx.se/libcurl/c/
4. 编译命令：

```bash
g++ virus.cpp -lcurl
```

当没有设置CURLOPT_WRITEFUNCTION和CURLOPT_WRITEDATA选项的时候，libcurl使用fwrite和stdout作为默认值，因此HTTP服务器返回的数据会打印到标准输出上。

```c++
#include <curl/curl.h> //包含libcurl库的头文件

int main()
{
	CURL* client = curl_easy_init();  //初始化libcurl库，创建客户端句柄，类似于fopen

	curl_easy_setopt(client, CURLOPT_URL, "http://coronavirus-tracker-api.herokuapp.com/v2/locations/225");  //设置要访问的REST API地址
    //curl_easy_setopt(client, CURLOPT_WRITEFUNCTION, fwrite);
    //curl_easy_setopt(client, CURLOPT_WRITEDATA, stdout);
	curl_easy_perform(client); //默认执行GET方法，并将服务器返回的数据打印到标准输出

	curl_easy_cleanup(client);  //释放客户端句柄，类似于fclose
}
```

将HTTP服务器返回的数据保存到字符串变量中

```c++
#include <curl/curl.h>
#include <string>
#include <iostream>

using namespace std;

//libcurl在处理服务器返回的数据时会多次调用回调函数将数据写入string对象
//ptr指针指向的内存保存服务返回的数据内容
//userdata就是指向string对象的指针
size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata)
{
    //将最后一个参数强制转换为string对象的指针
	string* jsonstr = static_cast<string*>(userdata);
	jsonstr->append(ptr, size*nmemb);
	return size*nmemb;
}

int main()
{
	CURL* client = curl_easy_init();
	string jsonstr;

	curl_easy_setopt(client, CURLOPT_URL, "http://coronavirus-tracker-api.herokuapp.com/v2/locations/225");
    //注册写入数据的回调函数
	curl_easy_setopt(client, CURLOPT_WRITEFUNCTION, write_callback);
    //设置回调函数的最后一个参数
	curl_easy_setopt(client, CURLOPT_WRITEDATA, &jsonstr);
	curl_easy_perform(client);
	//打印服务器返回的数据
	cout << jsonstr << endl;

	curl_easy_cleanup(client);
}

```

使用JSONCPP解析服务器返回的数据

jsoncpp代码库：https://github.com/open-source-parsers/jsoncpp

安装：

```bash
sudo apt install libjsoncpp-dev
```

```c++
#include <curl/curl.h>
#include <string>
#include <iostream>
#include <jsoncpp/json/json.h>  //jsoncpp的头文件

using namespace std;
using namespace Json;  //引用名字空间

size_t write_callback(char* ptr, size_t size, size_t nmemb, void* userdata)
{
	string* jsonstr = static_cast<string*>(userdata);
	jsonstr->append(ptr, size*nmemb);
	return size*nmemb;
}

int main()
{
	CURL* client = curl_easy_init();
	string jsonstr;

	curl_easy_setopt(client, CURLOPT_URL, "http://coronavirus-tracker-api.herokuapp.com/v2/locations/225");
	curl_easy_setopt(client, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(client, CURLOPT_WRITEDATA, &jsonstr);
	curl_easy_perform(client);

	//cout << jsonstr << endl;

	Value json; //保存解析后的json对象
	Reader parser;
	parser.parse(jsonstr, json); //解析服务器返回的json字符串

    //将json对象转换为字符串
	//StyledWriter writer;
	//cout << writer.write(json) << endl;

    //获取json对象中的确诊和死亡人数
	cout << "confirmed: " << json["location"]["latest"]["confirmed"] << endl;
	cout << "deaths: " << json["location"]["latest"]["deaths"] << endl;

	curl_easy_cleanup(client);
}

```

编译命令：

```bash
g++ virus.cpp -lcurl -ljsoncpp
```

## 调用百度人脸识别API

下载百度人脸识别C++ SDK：https://ai.baidu.com/download?sdkId=82

百度C++的在线SDK使用libcurl和jsoncpp访问其REST API，只做了简单的封装，所有代码都放在头文件中。

SDK解压后会创建一个aip-cpp-sdk目录，我们只需要包含其中的头文件就可以使用。

需要安装openssl的开发包

```bash
sudo apt install libssl-dev
```

在人脸检测代码的基础上进行修改：

```c++
#include <opencv2/opencv.hpp>
#include <vector>
#include <iostream>
#include "aip-cpp-sdk/face.h"

using namespace cv;
using namespace std;

int main()
{
	string app_id = "人脸识别的APP ID";
	string api_key = "人脸识别的API Key";
	string secret_key = "人脸识别的secret key";
	aip::Face client(app_id, api_key, secret_key);//创建人脸识别客户端

	VideoCapture cam(0);
	CascadeClassifier classifier("/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml");

	namedWindow("Camera");
	namedWindow("Face"); //显示发送给百度云进行识别的人脸区域

	Mat image;
	Mat gray;

	while(1)
	{
		cam >> image;
		cvtColor(image, gray, COLOR_BGR2GRAY);
		equalizeHist(gray, gray);
		vector<Rect> faces;
		classifier.detectMultiScale(gray, faces);
		if (faces.size())
		{
			rectangle(image, faces[0], CV_RGB(255, 0, 0));
			Mat face(image, faces[0]); //对原图像进行裁剪，仅保留人脸的矩形区域
			imshow("Face", face); //将人脸显示到小窗口
			vector<unsigned char> buf; //保存压缩后的人脸图片
			imencode(".jpg", face, buf); //将人脸图片压缩为jpeg格式
            //将jpeg格式的人脸图片编码为BASE64字符串
			string faceimg = aip::base64_encode((char*)buf.data(), buf.size());
            //将人脸图片发送给百度云进行识别，识别结果保存到json对象中
			Json::Value json = client.search(faceimg, "BASE64", "group1", aip::null);
			Json::StyledWriter writer;
            //将识别结果转换为json字符串，显示在标准输出上
			cout << writer.write(json) << endl;
		}
		imshow("Camera", image);
		if (waitKey(40) != 255)
		{
			break;
		}
	}
}

```

编译命令：

```bash
g++ face.cpp -I/usr/include/jsoncpp -lopencv_videoio -lopencv_core -lopencv_highgui -lopencv_objdetect -lopencv_imgproc -lopencv_imgcodecs -lcurl -ljsoncpp -lcrypto
```

## 性能优化

第一个迭代，只实现功能就可以了，性能优化，还有其他非功能性的需求，一般优先级比较低，放到后面的迭代阶段开发。

1. 不需要对采集到的每一帧图像进行人脸检测，可以进行抽样检测，比如：每秒钟只检测5帧图像。
2. 不需要每次人脸检测成功后，都去百度云进行人脸识别，可以进行抽样识别，比如：一秒钟连续5次检测出人脸，我们假设这5次检测出的人脸是同一个人的脸，此时只把最后一次检测出来的人脸发送到百度云进行人脸识别。（百度云API的每秒钟请求次数QPS=1）
3. 百度SDK的函数是一个同步函数（阻塞），需要等百度服务器返回人脸识别的结果后，才能继续运行，影响显示视频的帧数，可以修改为异步调用（自己实现HTTP客户端），也可以修改为多线程，一个线程负责视频采集，显示和人脸检测，另外一个线程负责对人脸图像进行压缩，转码，并调用百度API。
4. 通过QEaplsedTimer测量函数执行时间。

```mermaid
  sequenceDiagram
  	participant GUI as 界面显示（主线程）
  	participant Face as 人脸识别线程
  	loop 200ms
        GUI->>GUI: 获取摄像头的一帧图像
        GUI->>+Face: doDetect(一帧图像)
        GUI->>GUI: 显示一帧图像
    end
    loop 1s
        GUI->>GUI: 获取摄像头的一帧图像
        GUI->>Face: doRecognize(人脸图像)
        GUI->>GUI: 显示一帧图像
    end
    Face->>Face: 人脸检测（200ms）
    Face->>GUI: detected(人脸矩形区域)
    GUI->>GUI: 显示矩形框
    Face->>Face: 人脸识别（700ms）
    Face->>-GUI: recognized(员工信息)
    GUI->>GUI: 显示员工信息
```

1. 优化分类器参数：指定detectMultiScale函数的其他参数，可以缩小图像，可以限制人脸的最小大小，小于此小大的人脸不需要检测。
2. 替换人脸检测模型：/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml
3. 替换人脸检测算法：https://github.com/ShiqiYu/libfacedetection

## OpenCV显示文字

```c++
//此函数不支持中文字符的显示，显示中文字符可以使用cv::freetype::FreeType2::putText函数
putText(image, "liuyu", Point(10, 50), FONT_HERSHEY_SIMPLEX, 1.0,Scalar(0,0,255));
//image: 将文字放到哪个图像上
//liuyu: 要显示的文字
//point: 字符串的左下角在图像上的坐标
//font：字体
//字体大小，是一个缩放系数
//字体颜色，BGR

```

## 获取当前时间

time()函数返回从1970/1/1到当前时间的秒数。



## SQLite3数据库

1. 安装命令行工具和开发库

```bash
sudo apt install sqlite3 libsqlite3-dev
```

2. 通过代码操作数据库

```c++
#include <sqlite3.h>  //增加头文件

sqlite3* db;
// 打开数据库
if (sqlite3_open("attend.db", &db))
{
    cout << sqlite3_errmsg(db) << endl;
}

string user_id = "liuyu";
time_t now = time(nullptr); //获取当前时间，从1970/1/1到现在的秒数
char* errmsg;

//执行SQL语句
string sqlcmd = "insert into punch values (\"" + user_id + "\", " + to_string(now) + ")";
if (sqlite3_exec(db, sqlcmd.c_str(), NULL, NULL, &errmsg))
{
    cout << errmsg << endl;
}
else
{
    //打卡数据添加成功后在屏幕显示信息
    putText(image, "liuyu", Point(10, 50), FONT_HERSHEY_SIMPLEX, 
            1.0,Scalar(0,0,255));
}

sqlite3_close(db);

```

3. 编译命令

```bash
g++ face.cpp -I/usr/include/jsoncpp -lopencv_videoio -lopencv_core -lopencv_highgui -lopencv_objdetect -lopencv_imgproc -lopencv_imgcodecs -lcurl -ljsoncpp -lcrypto -lsqlite3
```

4. 查询数据

需要定义回调函数，sqlite3_exec每查询出一条记录，回调函数就会被调用一次

```c++
//data 通过sqlite3_exec传入的参数
//argc 查询出来字段的个数
//argv 每个字段的数据，字符串数组
//col  每个字段的名称，字符串数组
static int getname_cb(void* data, int argc, char** argv, char** col)
{
    string* value = reinterpret_cast<string*>(data);
    *value = argv[0];
    return 0;
}

int getname(string id, string& name)
{
    int rc;
    char* errmsg;

    string cmd = "select name from employee where id = \"" + id + "\"";
    rc = sqlite3_exec(db, cmd.c_str(), getname_cb, &name, &errmsg);
    if (rc != SQLITE_OK)
    {
        cout << errmsg << endl;
        sqlite3_free(errmsg);
        return 2;
    }
    return 0;
}

```



## pkg-config

pkg-config用来生成编译参数。

安装：

```bash
sudo apt install pkg-config
```

修改后的编译命令：

```bash
g++ face.cpp $(pkg-config opencv libcurl jsoncpp sqlite3 libcrypto --cflags --libs)
```

## OpenCV和QT混合编程

一般在商业产品中不会使用OpenCV的GUI函数创建界面，不灵活，控件少。

安装QT：

QT的商业版本支持交叉编译，在开源版本中不提供ARM平台的库，因此开源版本默认不支持Linux环境下的交叉编译。

1. 自己编译ARM版本的QT库。（时间长）
2. 直接在开发板上安装QT的本地编译环境。

```bash
sudo apt install qt5-default qtcreator
```

将OpenCV的Mat类型转换成QImage：

```c++
cv::cvtColor(image, image, CV_BGR2RGB); //将BGR格式转换为RGB
QImage qimage(image.data, image.cols, image.rows, image,step, QImage::Format_RGB888);
```

修改pro文件：

```makefile
CONFIG += link_pkgconfig
PKGCONFIG += opencv libcurl jsoncpp
```

## 安装中文输入法

```bash
sudo apt install ibus-libpinyin
```

## 应用层协议

传输层：TCP/TLS

TCP/IP协议的应用层（OSI：会话层+表示层+应用层）：

1. TCP连接建立时表示一次会话的开始，TCP连接断开时表示一次会话结束。
2. 可以使用XML/JSON最为表示层协议，有大量的解析库可以使用，不需要自己手写生成和解析的代码。
3. OSI的应用层，描述消息的字段和交互流程。

定义考勤机客户端和考勤服务器之间的协议：

认证：

```mermaid
  sequenceDiagram
  	考勤机客户端->>考勤服务器: 建立TCP连接
    考勤机客户端->>考勤服务器: 认证请求(考勤机ID，密码)
    考勤服务器->>考勤机客户端: 认证响应(成功/失败)
```

认证请求：

```JSON
{
    "cmd": "auth_req",
    "id": 1,
    "password": "abc"
}
```

认证响应：

```json
{
    "cmd": "auth_ack",
    "status": 0,
    "error": "OK"
}
```

TCP客户端：

```c++
QTcpSocket client;//创建TCP客户端对象
client.connectToHost(ip, port);//连接考勤服务器
//构造认证请求消息
Json::Value root;
root["cmd"] = "auth_req";
root["id"] = 1;
root["password"] = "abc";
Json::FastWriter writer;
cout << writer.write(root) << endl;

client.write(writer.write(root).c_str());//将消息发送给考勤服务器
```

TCP服务器：

```c
socket();
bind();
listen();
accept();
read();

Json::Value root;
Json::Reader reader;
reader.parse(jsonstr, root);

if (root["cmd"].asString() == "auth_req")
{
    //处理认证请求消息
    clientid = root["id"].asInt();
    ...
}
```

服务器如果不想使用C++，可以用cJSON库解析json消息。https://github.com/DaveGamble/cJSON

二进制图像可以转换为BASE64字符串进行传输。

二进制消息：一般使用TLV（type + length + value）格式描述消息中的一个字段

```c
struct field
{
    int type;
    int length;
    char value[];
};

```



## Qt多线程编程

使用线程的两种方式：

1. 继承QThread类，在子类中实现run方法。在创建线程时调用子类的start方法。在线程中不能处理事件。
2. 定义工作类继承QObject，创建线程时使用moveToThread方法将工作类与线程绑定。
3. 后台线程不能直接操作界面，因此我们要把人脸识别的结果通过信号发送给主线程进行处理。

connect函数的第5个参数表示信号和槽函数的连接方式：

![image-20200413113213145](function%20design.assets/image-20200413113213145.png)

直接连接：槽函数在信号发出后立即调用，与发出信号的函数在同一个线程中执行。

队列连接：发出信号的函数与槽函数不在同一个线程运行。

自动连接：如果发送信号的函数与槽函数在同一线程则使用直接连接，在不同线程则使用队列连接。

阻塞队列连接：同队列连接，但发出信号的线程在槽函数执行完成前一直阻塞。

如果使用队列连接，信号和槽函数的参数必须是QT的原生类型，如果不是QT的原生类型，需要将此类型注册到Qt的类型系统中给MOC（元对象编译器）使用。

```c++
//在头文件中增加声明
Q_DECLARE_METATYPE(Employee);

//在调用connect连接信号槽之前注册类型
qRegisterMetaType<Employee>("Employee");
```

## 邮件发送

SMTP（Simple Mail Transfer Protocol）

RFC5321协议

![image-20200415102235491](function%20design.assets/image-20200415102235491.png)

例子：

https://gitee.com/tinytaro/codes/0gnrimhj342scqvfleyp851

## 项目扩展

人脸识别闸机：（小区，写字楼，学校）控制外来人员进入，相比人脸考勤，增加了闸机的控制，通过继电器控制闸门的开关。检查进出人员是否佩戴口罩（图像分类，调用百度EasyDL的API识别是否佩戴口罩），如果没有戴口罩，进行人脸识别，进行记录。

课程签到系统：在教室中放置摄像头，拍摄照片，将所有的人脸都检测出来，依次进行人脸识别并签到。